# Transito

[![builds.sr.ht status](https://builds.sr.ht/~mil/transito.svg)](https://builds.sr.ht/~mil/transito?)

Transito is a simple UI (currently supporting Android & Linux)
for using [Mobroute](http://sr.ht/~mil/mobroute), the minimal
FOSS public-transportation router, on-the-go. The end goal is to
have a simple app that allows you to generate routes (sequences
of steps - like board this train at this time, stay on for these
stops, transfers here or there) between 2 geolocations. As far as
implementation goes, this app is essentially just a thin wrapper
built out in [Gio](https://gioui.org/) over Mobroute's Go API.
[Mobroute](http://git.sr.ht/~mil/mobroute) itself (the core routing
algorithm), and [Mobsql](http://git.sr.ht/~mil/mobsql) (it's backing GTFS
ingestion ETL flow) are both in active development. This project serves
as a testbed for users who don't want to use the Mobroute commandline
but want to experiment with Mobroute on-the-go (Android & Mobile Linux)
in a simple fashion.

Currently the UI is built out / designed in a deliberately 'functional'
way, and being a testing utility primarily, is not intended to be
"pretty". Do note, Transito is currently in active development along with
[Mobroute](http://sr.ht/~mil/mobroute) project overall. As such this
project is largely aimed at early adopters, should be considered in a
'pre-alpha', and largely aimed at those who want to help with testing
and development at the moment.

**Overview of Functionality:**

- Search for points to route from/to via Nominatim
- Select an location via select box
- Set route from/to via buttons
- Allow user to change tunable Mobroute parameters 
- Route button calls underlying Mobroute route library call
- Show result JSON for Mobroute call
- Show elapsed time for calls to Nominatim/Mobroute

## **Build & Install**

Eventually the plan is to distribute this app via F-droid; and similarly
with a package (on Alpine Linux). However currently, as this is very
much a project in infancy, you need to build things locally, see below
for steps.

Build for Linux (X11/Wayland):
- On Linux, there are a few dependencies to first install
  - See [Gio's Linux Guide](https://gioui.org/doc/install/linux)
  - Dependencies on Alpine Linux: `apk add libxcursor-dev libxkbcommon-dev libxkbcommon-x11 mesa-egl vulkan-headers wayland-dev mesa-dev xdg-utils`
- Once depenencies are setup, run:
  - `./build.sh blinux`
- Run via `./transito`

Build for Android:
- (Note: you can use a prebaked APK if you don't wish to build locally for
   Android, see below section)
- On Android you'll need to have the Android SDK setup
  - See [Gio's Android Guide](https://gioui.org/doc/install/android)
- Note: Android 11 / R / API Version 30 & SQLite version >= 3.28.0 is
  required.
- Once dependencies setup, run:
  - `./build.sh bandroid`
- Once build is complete, you have a new `.apk`:
  - Run `adb install transito.apk` to copy to device
  - Or copy the apk to your device some other way

## **Android APK generated via CI**

An installable Android APK is built on publishing each tag and also on
each push to the master branch.

You can download the latest APK at:
[https://ci.lrdu.org/android/](https://ci.lrdu.org/android/)

## **Usage**
Essentially the app is broken up into three pages, route, search, and
config. See below screenshots for a visual representation.

- **Route:** *(left top screenshot)*
  - This is the primary view that allows you to Mobroute execute routing requests.
  - Clicking the From/To labels take you to the search page letting you change these fields
  - Once From/To is set, you can click on the coordinates (in blue) to open the coordinates as a geocordinate (`geo://`) link
  - Change the parameters as desired, explanation of each parameter
    - **Max Wᵐ**: Max walk minutes
    - **Max Tᵐ**: Max total trip minutes
    - **Min Cᵐ**: Min change transfer minutes
    - **Max Cᵐ**: Max change transfer minutes
    - **Wᵏᵐʰʳ**: Walkspeed km/hr
  - Press swap if you want to interchange the from/to locations
  - Press route once you have your parameters set as desired
  - The JSON result for the Mobroute routing request will appear below
  - In future versions, the result will be styled; for now being more of a
    debugging tool, only JSON is provided
- **Search:** *(right top screenshot)*
  - In the text input field "Nominatim Search" you can search for a location
    by entering a query; this can be a street address or a point of intrest
  - Note this search is bounded in Nominatim by the bounding box for the MDBID source (see Config page)
  - Press the "Ssearch" button to search
  - The results will come back in the list below
  - Select a result and press either "Set as From" to set the from field for the
    routing requesting or "Set as To" to set the to field for the routing request
- **Config:** *(left bottom screenshot)*
  - Configuration Section
    - This section allows you to change configuration parameters effecting routing requests
    - The Use Sources field controls which sources are used for both routing requests and Nominatim searches
    - Change the Use Sources field to the numerical MDBID source(s) as desired. Information about the sources chosen will appear below in the table
    - The checkboxes are as follows
      - **Ttbl**: Whether to use transfers.txt table
      - **Tcmp**: Whether to use computed/generated transfers
      - **Verbose**: Whether to show verbose performance statistics in JSON response
  - Search Sources Section
      - This section allow you to search the [Mobility Database](https://github.com/MobilityData/mobility-database-catalogs) for sources by region, provider, name, etc.
      - Enter your search phrase into the "Search Sources" field
      - Results will appear in the table below
      - Based on the response sources, you can plug the resulting MDBIDs into the above "Use Sources" field to change routing GTFS source.
 

## **Screenshots**

| | |
|--|---|
| Route | Search |
| ![Transito Screenshot Route](doc/transito_screenshot_route.png)  |  ![Transito Screenshot B](doc/transito_screenshot_search.png) |
| Config|  |
| ![Transito Screenshot Config](doc/transito_screenshot_config.png)  |   |

