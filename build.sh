#!/usr/bin/env sh
set -xe

fmt() {
  ag -g '.go$' | xargs -IC go fmt C
}

tmpfsgodev() {
  USER=u
  mount -t tmpfs gopkg /home/$USER/go/pkg
  mount -t tmpfs gocache /home/$USER/.cache/go-build
}

bandroid() {
  gogio -tags=sqlite_math_functions -target android -ldflags="-X git.sr.ht/~mil/transito/uipages/pageconfig.Commit=$(git rev-parse HEAD)" .
}

blinux() {
  go build -v -tags=sqlite_math_functions -ldflags="-X git.sr.ht/~mil/transito/uipages/pageconfig.Commit=$(git rev-parse HEAD)"
}

adbinstall() {
  adb uninstall ht.sr.git.transito || echo "uninstall fail"
  adb install transito.apk
}

adbflush() {
  adb kill-server
  adb start-server
}

updatemr() {
  GOPROXY=direct go get -v -u git.sr.ht/~mil/mobroute
}

um() {
  updatemr
}

cpapk() {
  MPOINT="/mnt/androidusb"
  DISK="/dev/disk/by-label/ANDROIDUSB"
  APK="transito.apk"

  mkdir -p "$MPOINT"
  mount "$DISK" "$MPOINT"
  cp "$APK" "$MPOINT"/
  umount "$MPOINT"
  echo "Copied apk $APK ok to $DISK"
}

acp() {
  bandroid
  doas "$0" cpapk
}

run() {
  blinux
  ./transito
}

"$@"