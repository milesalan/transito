package connmobroute

import (
	"git.sr.ht/~mil/mobroute/api/apidb"
	"git.sr.ht/~mil/transito/utilfuncs"
)

func MobrouteExecuteDBStatusMDBIDs(csv string) ([]apidb.SourceStatus, error) {
	var (
		mdbids, _ = utilfuncs.ParseCsvToInts(csv)
	)
	if len(mdbids) == 0 {
		// Note by default empty filter in mobsql sourceset filter passes
		// through entire / all sources - this is not really desirable on UI side
		return []apidb.SourceStatus{}, nil
	} else {
		return apidb.CommandDBStatus(&apidb.DBCommandParams{
			LoadFilter: &apidb.SourcesetFilter{Mdbid: mdbids},
		})
	}
}
