package connmobroute

import (
	"git.sr.ht/~mil/mobroute/api/apiroute"
)

type MobrouteRequest struct {
	Sources            string
	MaxWalkMinutes     string
	MaxTripMinutes     string
	MinTransferMinutes string
	MaxTransferMinutes string
	WalkspeedKmHr      string
	LatLonFrom         []float64
	LatLonTo           []float64
	TransfersTable     bool
	TransfersGenerated bool
}

type MobrouteResponse = apiroute.RouteResponse
