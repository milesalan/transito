package connmobroute

import (
	"strconv"
)

func parseUint(str string) uint {
	if v, err := strconv.Atoi(str); err != nil {
		return 0
	} else {
		return uint(v)
	}
}

func parseFloat64(str string) float64 {
	if v, err := strconv.ParseFloat(str, 10); err != nil {
		return 0.0
	} else {
		return v
	}
}

func ptr[T any](a T) *T {
	return &a
}
