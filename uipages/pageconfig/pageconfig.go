package pageconfig

import (
	"fmt"
	"gioui.org/layout"
	"gioui.org/unit"
	"gioui.org/widget"
	"gioui.org/widget/material"
	"gioui.org/x/component"
	"gioui.org/x/richtext"
	"git.sr.ht/~mil/mobroute/api/apidb"
	"git.sr.ht/~mil/transito/connmobroute"
	"git.sr.ht/~mil/transito/uicomponents/divider"
	"git.sr.ht/~mil/transito/uicomponents/tableview"
	"git.sr.ht/~mil/transito/uistate"
	"git.sr.ht/~mil/transito/utillinkhandler"
	"strings"
)

type PageConfig struct {
	UIState *uistate.UIState

	TextState richtext.InteractiveText
	spans     []richtext.SpanStyle

	// Basic params
	usesourcesCached    string
	usesourcesTextfield component.TextField
	usesourceTable      tableview.TableView
	usesourcesTableData []tableview.TableRow

	// Advanced params
	checkboxTransfersTable      widget.Bool
	checkboxTransfersComputed   widget.Bool
	checkboxVerbose             widget.Bool
	textfieldMaxWalkMinutes     component.TextField
	textfieldMaxTripMinutes     component.TextField
	textfieldMaxTransferMinutes component.TextField
	textfieldMinTransferMinutes component.TextField
	textfieldWalkspeedKmHr      component.TextField

	list widget.List
}

func Initialize(u *uistate.UIState) PageConfig {
	widget := PageConfig{
		UIState: u,

		list: widget.List{List: layout.List{Axis: layout.Vertical}},

		usesourcesTextfield: component.TextField{Editor: widget.Editor{Filter: "1234567890,"}},
		usesourcesTableData: []tableview.TableRow{},
		usesourceTable: tableview.TableView{
			Theme:  u.Theme,
			Labels: []string{"Source ID", "Provider", "Feed", "Loaded"},
		},

		textfieldMaxWalkMinutes:     component.TextField{Editor: widget.Editor{Filter: "1234567890"}},
		textfieldMaxTripMinutes:     component.TextField{Editor: widget.Editor{Filter: "1234567890"}},
		textfieldMaxTransferMinutes: component.TextField{Editor: widget.Editor{Filter: "1234567890"}},
		textfieldMinTransferMinutes: component.TextField{Editor: widget.Editor{Filter: "1234567890"}},
		textfieldWalkspeedKmHr:      component.TextField{Editor: widget.Editor{Filter: "1234567890."}},
	}

	// Defaults
	widget.usesourcesTextfield.SetText(u.Sources)
	widget.checkboxTransfersTable.Value = true
	widget.checkboxTransfersComputed.Value = true
	widget.textfieldMaxWalkMinutes.SetText("10")
	widget.textfieldMaxTripMinutes.SetText("240")
	widget.textfieldMinTransferMinutes.SetText("3")
	widget.textfieldMaxTransferMinutes.SetText("20")
	widget.textfieldWalkspeedKmHr.SetText("4.5")

	for _, tf := range []*component.TextField{
		&widget.textfieldMaxWalkMinutes,
		&widget.textfieldMaxTripMinutes,
		&widget.textfieldMinTransferMinutes,
		&widget.textfieldMaxTransferMinutes,
		&widget.textfieldWalkspeedKmHr,
	} {
		tf.MoveCaret(9999, 99999)
	}

	if rendered, err := widget.UIState.MarkdownRenderer.Render([]byte(fmt.Sprintf(
		`*Confused, about which source(s) to use or how to use the advanced configuration? See the [Manual](%s).*

**Build:** [%s](%s)
		`,
		"https://git.sr.ht/~mil/transito#strongusagestrong",
		truncate(Commit, 8),
		"https://git.sr.ht/~mil/transito",
	))); err == nil {
		widget.spans = rendered
	}

	widget.SyncStateToUIState()

	return widget
}

func (widget *PageConfig) SyncStateToUIState() {
	widget.UIState.MaxWalkMinutes = widget.textfieldMaxWalkMinutes.Text()
	widget.UIState.MaxTripMinutes = widget.textfieldMaxTripMinutes.Text()
	widget.UIState.MinTransferMinutes = widget.textfieldMinTransferMinutes.Text()
	widget.UIState.MaxTransferMinutes = widget.textfieldMaxTransferMinutes.Text()
	widget.UIState.WalkspeedKmHr = widget.textfieldWalkspeedKmHr.Text()
	widget.UIState.TransfersComputed = widget.checkboxTransfersComputed.Value
	widget.UIState.TransfersTable = widget.checkboxTransfersTable.Value
	widget.UIState.VerboseOutput = widget.checkboxVerbose.Value
}

func (widget *PageConfig) Render(gtx layout.Context) layout.Dimensions {
	utillinkhandler.HandleLinkEvents(widget.UIState.Window, &widget.TextState)

	widget.SyncStateToUIState()

	// Sync state out to global UIState object
	if widget.usesourcesCached != widget.UIState.Sources {
		widget.UIState.Sources = widget.usesourcesCached
	}

	// Update out of date usesources table data
	if widget.usesourcesCached != widget.usesourcesTextfield.Text() {
		go widget.updateUsesourcesTable()
	}

	widgetCheckboxes := func(gtx layout.Context) layout.Dimensions {
		gtx.Constraints.Min.Y = gtx.Dp(unit.Dp(40))
		gtx.Constraints.Max.Y = gtx.Constraints.Min.Y
		return layout.Flex{}.Layout(gtx, layout.Rigid(dimensionsToWidget(layout.Flex{}.Layout(
			gtx,
			layout.Flexed(0.2, material.CheckBox(widget.UIState.Theme, &widget.checkboxTransfersTable, "Ttbl").Layout),
			layout.Flexed(0.01, layout.Spacer{Width: unit.Dp(2)}.Layout),
			layout.Flexed(0.2, material.CheckBox(widget.UIState.Theme, &widget.checkboxTransfersComputed, "Tcmp").Layout),
			layout.Flexed(0.01, layout.Spacer{Width: unit.Dp(2)}.Layout),
			layout.Flexed(0.2, material.CheckBox(widget.UIState.Theme, &widget.checkboxVerbose, "Verbose").Layout),
		))))
	}

	// Route params
	widgetTextfieldMaxWalkMinutes := func(gtx layout.Context) layout.Dimensions {
		gtx.Constraints.Min.Y = gtx.Dp(unit.Dp(15))
		widget.textfieldMaxWalkMinutes.SingleLine = true
		return widget.textfieldMaxWalkMinutes.Layout(gtx, widget.UIState.Theme, "Max Wᵐ")
	}
	widgetTextfieldMaxTripMinutes := func(gtx layout.Context) layout.Dimensions {
		gtx.Constraints.Min.Y = gtx.Dp(unit.Dp(15))
		widget.textfieldMaxTripMinutes.SingleLine = true
		return widget.textfieldMaxTripMinutes.Layout(gtx, widget.UIState.Theme, "Max Tᵐ")
	}
	widgetTextfieldMinTransferMinutes := func(gtx layout.Context) layout.Dimensions {
		gtx.Constraints.Min.Y = gtx.Dp(unit.Dp(15))
		widget.textfieldMinTransferMinutes.SingleLine = true
		return widget.textfieldMinTransferMinutes.Layout(gtx, widget.UIState.Theme, "Min Cᵐ")
	}
	widgetTextfieldMaxTransferMinutes := func(gtx layout.Context) layout.Dimensions {
		gtx.Constraints.Min.Y = gtx.Dp(unit.Dp(15))
		widget.textfieldMaxTransferMinutes.SingleLine = true
		return widget.textfieldMaxTransferMinutes.Layout(gtx, widget.UIState.Theme, "Max Cᵐ")
	}
	widgetTextfieldWalkspeedKmHr := func(gtx layout.Context) layout.Dimensions {
		gtx.Constraints.Min.Y = gtx.Dp(unit.Dp(15))
		widget.textfieldWalkspeedKmHr.SingleLine = true
		return widget.textfieldWalkspeedKmHr.Layout(gtx, widget.UIState.Theme, "Wᵏᵐʰʳ")
	}

	widgetRouteNumbersRow := func(gtx layout.Context) layout.Dimensions {
		gtx.Constraints.Min.Y = gtx.Dp(unit.Dp(40))
		gtx.Constraints.Max.Y = gtx.Constraints.Min.Y
		return layout.Flex{}.Layout(gtx, layout.Rigid(dimensionsToWidget(layout.Flex{}.Layout(
			gtx,
			layout.Flexed(0.2, widgetTextfieldMaxWalkMinutes),
			layout.Flexed(0.2, widgetTextfieldMaxTripMinutes),
			layout.Flexed(0.2, widgetTextfieldMinTransferMinutes),
			layout.Flexed(0.2, widgetTextfieldMaxTransferMinutes),
			layout.Flexed(0.2, widgetTextfieldWalkspeedKmHr),
		))))
	}

	widgetsScroll := []layout.Widget{
		// Basic Configuration Section
		material.H6(widget.UIState.Theme, "Basic Configuration").Layout,
		func(gtx layout.Context) layout.Dimensions {
			gtx.Constraints.Min.Y = gtx.Dp(unit.Dp(15))
			widget.usesourcesTextfield.SingleLine = true
			return widget.usesourcesTextfield.Layout(gtx, widget.UIState.Theme, "Use Sources")
		},
		func(gtx layout.Context) layout.Dimensions {
			return widget.usesourceTable.Layout(gtx, widget.usesourcesTableData)
		},

		// Advanced Configuration Section
		divider.Divider(widget.UIState.Theme).Layout,
		material.H6(widget.UIState.Theme, "Advanced Configuration").Layout,
		widgetRouteNumbersRow,
		widgetCheckboxes,

		// Help Section
		divider.Divider(widget.UIState.Theme).Layout,
		material.H6(widget.UIState.Theme, "Help").Layout,
		richtext.Text(&widget.TextState, widget.UIState.Theme.Shaper, widget.spans...).Layout,
	}
	return material.List(widget.UIState.Theme, &widget.list).Layout(gtx, len(widgetsScroll), func(gtx layout.Context, i int) layout.Dimensions {
		return layout.UniformInset(unit.Dp(16)).Layout(gtx, widgetsScroll[i])
	})
}

func (widget *PageConfig) updateUsesourcesTable() {
	results, _ := connmobroute.MobrouteExecuteDBStatusMDBIDs(
		widget.usesourcesTextfield.Text(),
	)
	widget.usesourcesTableData = *mapResultIntoTableResults(&results)
	widget.usesourcesCached = widget.usesourcesTextfield.Text()
}

func mapResultIntoTableResults(results *[]apidb.SourceStatus) *[]tableview.TableRow {
	returnTableData := []tableview.TableRow{}
	if len(*results) > 30 {
		*results = (*results)[0:30]
	}
	for _, r := range *results {
		nonNilLocationFields := []string{}
		for _, l := range []*string{
			r.Name, r.LocationMunicipality, r.LocationSubdivision, &r.LocationCountry,
		} {
			if l != nil {
				nonNilLocationFields = append(nonNilLocationFields, *l)
			}
		}
		locationField := strings.Join(nonNilLocationFields, " / ")
		returnTableData = append(returnTableData, tableview.TableRow{
			Datum: []string{
				fmt.Sprintf("%d", r.Mdbid),
				r.Provider,
				locationField,
				fmt.Sprintf("%v", r.Loaded),
			},
		})
	}
	return &returnTableData
}
