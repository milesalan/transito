package pagesearch

import (
	"fmt"
	"time"

	"gioui.org/io/key"
	"gioui.org/layout"
	"git.sr.ht/~mil/transito/connhttp"
)

func (widget *PageSearch) clickHandlers(gtx layout.Context) {
	if widget.clickableSearch.Clicked(gtx) {
		key.SoftKeyboardOp{Show: false}.Add(gtx.Ops)
		go func() {
			now := time.Now()
			widget.TimerStartNominatim = &now
			defer func() {
				widget.TimerStartNominatim = nil
				widget.UIState.Window.Invalidate()
			}()

			if result, err := connhttp.FetchNominatim(
				widget.textfieldSearch.Text(),
				widget.UIState.Sources,
			); err != nil {
				widget.selectListItems = []string{
					fmt.Sprintf("Nominatim Fetch error: %v", err),
				}
			} else {
				widget.selectListItems = []string{}
				for _, item := range *result {
					widget.selectListItems = append(widget.selectListItems, fmt.Sprintf(
						"%s - Latlon: %s %s",
						item.DisplayName, item.Lat, item.Lon,
					))
				}
			}
		}()
	}

	if widget.clickableSetFrom.Clicked(gtx) {
		if widget.selectList.Selected <= len(widget.selectListItems)-1 {
			if label, coords, err := parseLatLon(widget.selectListItems[widget.selectList.Selected]); err == nil {
				widget.UIState.LocationFromCoords = coords
				widget.UIState.LocationFromLabel = label
			}
		}
		widget.UIState.TabSelected = 0
		widget.UIState.Window.Invalidate()

		// Clear search field & results
		widget.textfieldSearch.Clear()
		widget.selectListItems = []string{}
	}

	if widget.clickableSetTo.Clicked(gtx) {
		if widget.selectList.Selected <= len(widget.selectListItems)-1 {
			if label, coords, err := parseLatLon(widget.selectListItems[widget.selectList.Selected]); err == nil {
				widget.UIState.LocationToCoords = coords
				widget.UIState.LocationToLabel = label
			}
		}
		widget.UIState.TabSelected = 0
		widget.UIState.Window.Invalidate()

		// Clear search field & results
		widget.textfieldSearch.Clear()
		widget.selectListItems = []string{}
	}
}
