package pagesearch

import (
	"errors"
	"gioui.org/layout"
	"strconv"
	"strings"
)

func wgToWidget(widget *PageSearch, wg func(widget *PageSearch, gtx layout.Context) layout.Dimensions) func(layout.Context) layout.Dimensions {
	return func(gtx layout.Context) layout.Dimensions {
		return wg(widget, gtx)
	}
}

func dimensionsToWidget(d layout.Dimensions) func(gtx layout.Context) layout.Dimensions {
	return func(gtx layout.Context) layout.Dimensions {
		return d
	}
}

func parseLatLon(str string) (string, []float64, error) {
	splt := strings.Split(str, " - Latlon: ")
	if len(splt) < 2 {
		return "", nil, errors.New("Couldn't find 'Latlon:' text")
	}
	coords := strings.Split(splt[1], " ")
	if lat, err := strconv.ParseFloat(strings.TrimSpace(coords[0]), 64); err != nil {
		return "", nil, err
	} else if lon, err := strconv.ParseFloat(strings.TrimSpace(coords[1]), 64); err != nil {
		return "", nil, err
	} else {
		return splt[0], []float64{lat, lon}, nil
	}
}
