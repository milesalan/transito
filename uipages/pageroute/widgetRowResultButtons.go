package pageroute

import (
	"image/color"
	"log"

	"gioui.org/layout"
	"gioui.org/unit"
	"gioui.org/widget/material"
	"github.com/inkeliz/giohyperlink"
)

func (page *PageRoute) widgetRowResultButtons(gtx layout.Context) layout.Dimensions {
	if page.ResultMobrouteError != nil {
		gtx.Constraints.Min.Y = gtx.Dp(unit.Dp(0))
		gtx.Constraints.Max.Y = gtx.Constraints.Min.Y
		return layout.Flex{}.Layout(gtx)
	}

	gtx.Constraints.Min.Y = gtx.Dp(unit.Dp(35))
	gtx.Constraints.Max.Y = gtx.Constraints.Min.Y

	buttonMap := material.Button(buttonTheme(*page.UIState.Theme, "green"), &page.clickableMap, "Map")

	buttonOverview := material.Button(
		buttonTheme(*page.UIState.Theme, fif(page.resultTabname == "overview", "blue", "gray")),
		&page.clickableOverview,
		"Overview",
	)
	buttonDbg := material.Button(
		buttonTheme(*page.UIState.Theme, fif(page.resultTabname == "dbg", "blue", "gray")),
		&page.clickableDbg,
		"Dbg",
	)
	buttonReq := material.Button(
		buttonTheme(*page.UIState.Theme, fif(page.resultTabname == "req", "blue", "gray")),
		&page.clickableReq,
		"Req",
	)

	// Switch to result tab logic
	if page.clickableOverview.Clicked(gtx) {
		page.resultTabname = "overview"
	}
	if page.clickableDbg.Clicked(gtx) {
		page.resultTabname = "dbg"
	}
	if page.clickableReq.Clicked(gtx) {
		page.resultTabname = "req"
	}

	// Map button treated specially to just open the map
	if page.clickableMap.Clicked(gtx) {
		if err := giohyperlink.Open(*page.ResultMobrouteResponse.MapURL); err != nil {
			log.Println("Error opening map URL:", err)
		}
	}

	return layout.Flex{}.Layout(gtx, layout.Rigid(dimensionsToWidget(layout.Flex{}.Layout(
		gtx,

		layout.Flexed(0.3, buttonOverview.Layout),
		layout.Flexed(0.01, layout.Spacer{Width: unit.Dp(2)}.Layout),
		layout.Flexed(0.3, buttonDbg.Layout),
		layout.Flexed(0.01, layout.Spacer{Width: unit.Dp(2)}.Layout),
		layout.Flexed(0.3, buttonReq.Layout),
		layout.Flexed(0.5, layout.Spacer{Width: unit.Dp(2)}.Layout),
		layout.Flexed(0.3, buttonMap.Layout),
	))))
}

func buttonTheme(baseTheme material.Theme, buttonColor string) *material.Theme {
	returnTheme := baseTheme
	returnTheme.TextSize = 14

	bgColor := color.NRGBA{R: 0xed, G: 0xed, B: 0xed, A: 0xFF}
	if buttonColor == "red" {
		bgColor = color.NRGBA{R: 0xff, G: 0xbf, B: 0xbf, A: 0xFF}
	} else if buttonColor == "blue" {
		bgColor = color.NRGBA{R: 0xe6, G: 0xf4, B: 0xff, A: 0xFF}
	} else if buttonColor == "green" {
		bgColor = color.NRGBA{R: 0xc2, G: 0xff, B: 0xd4, A: 0xFF}
	}

	returnTheme.Palette = material.Palette{
		Bg:         color.NRGBA{R: 0x00, G: 0x00, B: 0x00, A: 0xFF},
		Fg:         color.NRGBA{R: 0x00, G: 0x00, B: 0x00, A: 0xFF},
		ContrastBg: bgColor,
		ContrastFg: color.NRGBA{R: 0x00, G: 0x00, B: 0x00, A: 0xFF},
	}
	return &returnTheme
}

func fif(b bool, ifTrue, ifFalse string) string {
	if b {
		return ifTrue
	} else {
		return ifFalse
	}
}
