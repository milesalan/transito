package pageroute

import (
	"gioui.org/layout"
)

func dimensionsToWidget(d layout.Dimensions) func(gtx layout.Context) layout.Dimensions {
	return func(gtx layout.Context) layout.Dimensions {
		return d
	}
}
