package pageroute

import (
	"fmt"
	"git.sr.ht/~mil/mobroute/api/apiroute/formatterlegs"
	"git.sr.ht/~mil/transito/connmobroute"
	"strconv"
	"strings"
)

func overviewFormatMarkdown(mrResponse *connmobroute.MobrouteResponse) string {
	steps := ""
	for legI, leg := range *mrResponse.RouteLegs {
		stepText := legToStepText(&leg, legI)
		steps = fmt.Sprintf(
			"%s- %s\n",
			steps,
			stepText,
		)
	}

	return fmt.Sprintf(
		"###### **Steps** ‑ %dm, %dt\n%s",
		mrResponse.Overview.RoutediagSTotal/60,
		mrResponse.Overview.RoutediagNTransfers,
		steps,
	)
}

func legToStepText(leg *formatterlegs.RouteLeg, legI int) string {
	if leg.LegType == "walk" {
		return fmt.Sprintf(
			"%s: **Walk** :: [%s](%s) → [%s](%s) (%s)",
			timeTruncate(leg.LegBeginTime),

			*leg.WalkFrom,
			latLonToGeoURI(leg.LegFromCoords),

			*leg.WalkTo,
			latLonToGeoURI(leg.LegToCoords),

			leg.LegDuration,
		)
	} else if leg.LegType == "trip" {
		return fmt.Sprintf(
			"%s: **Trip** :: [%s](%s) → [%s](%s) (via %s toward %s) (%d stops / %s)",
			timeTruncate(leg.LegBeginTime),

			*leg.TripFrom,
			latLonToGeoURI(leg.LegFromCoords),

			*leg.TripTo,
			latLonToGeoURI(leg.LegToCoords),

			*leg.TripRoute,
			*leg.TripRouteHeadsign,

			len(*leg.TripStops),
			leg.LegDuration,
		)
	} else if leg.LegType == "transfer" {
		return fmt.Sprintf(
			"%s: **Transfer** :: [%s](%s) → [%s](%s) (%s)",
			timeTruncate(leg.LegBeginTime),

			*leg.TransferFrom,
			latLonToGeoURI(leg.LegFromCoords),

			*leg.TransferTo,
			latLonToGeoURI(leg.LegToCoords),

			leg.LegDuration,
		)
	}

	return ""
}

func latLonToGeoURI(latlonStr string) string {
	items := strings.Split(latlonStr, ",")
	latStr := strings.TrimSpace(items[0])
	lonStr := strings.TrimSpace(items[1])
	lat, _ := strconv.ParseFloat(latStr, 64)
	lon, _ := strconv.ParseFloat(lonStr, 64)

	return fmt.Sprintf("geo://%s,%s", lon, lat)
}

func timeTruncate(timeStr string) string {
	if len(timeStr) > 5 {
		return timeStr[0:5]
	} else {
		return timeStr
	}
}
