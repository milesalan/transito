package pageroute

import (
	"encoding/json"
	"fmt"

	"gioui.org/layout"
	"gioui.org/x/richtext"
)

func (page *PageRoute) widgetRowResultMarkdown(gtx layout.Context) layout.Dimensions {
	if spans, err := page.UIState.MarkdownRenderer.Render([]byte(
		func() string {
			if page.ResultMobrouteError != nil {
				return fmt.Sprintf("%v", page.ResultMobrouteError)
			} else if page.resultTabname == "overview" {
				return overviewFormatMarkdown(page.ResultMobrouteResponse)
			} else if page.resultTabname == "dbg" {
				textBytes, _ := json.MarshalIndent(page.ResultMobrouteResponse.Diagnostics, "", "  ")
				return "```\n" + string(textBytes) + "\n```"
			} else if page.resultTabname == "req" {
				textBytes, _ := json.MarshalIndent(page.ResultMobrouteResponse.Request, "", "  ")
				return "```\n" + string(textBytes) + "\n```"
			} else {
				return page.resultTabname
			}
		}(),
	)); err == nil {
		return richtext.Text(
			&page.richtextResultArea, page.UIState.Theme.Shaper, spans...,
		).Layout(gtx)
	} else {
		return richtext.Text(
			&page.richtextResultArea, page.UIState.Theme.Shaper,
		).Layout(gtx)
	}
}
