package pageroute

import (
	"errors"
	"fmt"
	"time"

	"gioui.org/font/gofont"
	"gioui.org/layout"
	"gioui.org/text"
	"gioui.org/unit"
	"gioui.org/widget"
	"gioui.org/widget/material"
	"gioui.org/x/richtext"
	"git.sr.ht/~mil/transito/connmobroute"
	"git.sr.ht/~mil/transito/uicomponents/divider"
	"git.sr.ht/~mil/transito/uistate"
	"git.sr.ht/~mil/transito/utillinkhandler"
)

type PageRoute struct {
	UIState *uistate.UIState
	list    widget.List

	richtextFrom       richtext.InteractiveText
	richtextTo         richtext.InteractiveText
	richtextResultArea richtext.InteractiveText

	ResultMobrouteResponse *connmobroute.MobrouteResponse
	ResultMobrouteError    error

	clickableRoute widget.Clickable
	clickableSwap  widget.Clickable

	clickableOverview widget.Clickable
	clickableMap      widget.Clickable
	clickableDbg      widget.Clickable
	clickableReq      widget.Clickable

	resultTabname string

	TimerStartMobroute *time.Time
}

func Initialize(u *uistate.UIState) PageRoute {
	page := PageRoute{
		UIState:                u,
		list:                   widget.List{List: layout.List{Axis: layout.Vertical}},
		ResultMobrouteResponse: nil,
		ResultMobrouteError:    errors.New("Result will show here"),
		TimerStartMobroute:     nil,
		resultTabname:          "overview",
	}
	page.UIState.Theme.Shaper = text.NewShaper(text.WithCollection(gofont.Collection()))
	return page
}

func (page *PageRoute) Render(gtx layout.Context) layout.Dimensions {
	utillinkhandler.HandleLinkEvents(page.UIState.Window, &page.richtextResultArea)

	fromSwitchTabClicked := utillinkhandler.HandleLinkEvents(page.UIState.Window, &page.richtextFrom) == "tab1"
	toSwitchTabClicked := utillinkhandler.HandleLinkEvents(page.UIState.Window, &page.richtextTo) == "tab1"
	if fromSwitchTabClicked || toSwitchTabClicked {
		page.UIState.TabSelected = 1
		page.UIState.Window.Invalidate()
	}

	// Update result area with mobroute timer value when running
	if page.TimerStartMobroute != nil {
		page.ResultMobrouteError = errors.New(fmt.Sprintf(
			"%.1fs",
			time.Now().Sub(*page.TimerStartMobroute).Seconds(),
		))
		page.UIState.Window.Invalidate()
	}

	widgetsScroll := []layout.Widget{
		page.widgetRowSplitToFrom,
		page.widgetRowActionButtons,
		divider.Divider(page.UIState.Theme).Layout,
		page.widgetRowResultButtons,
		page.widgetRowResultMarkdown,
	}

	return material.List(page.UIState.Theme, &page.list).Layout(
		gtx,
		len(widgetsScroll),
		func(gtx layout.Context, i int) layout.Dimensions {
			return layout.UniformInset(unit.Dp(16)).Layout(gtx, widgetsScroll[i])
		},
	)
}
