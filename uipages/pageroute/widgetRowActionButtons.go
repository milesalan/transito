package pageroute

import (
	"time"

	"gioui.org/layout"
	"gioui.org/unit"
	"gioui.org/widget/material"

	"git.sr.ht/~mil/transito/connmobroute"
)

func (page *PageRoute) widgetRowActionButtons(gtx layout.Context) layout.Dimensions {
	gtx.Constraints.Min.Y = gtx.Dp(unit.Dp(40))
	gtx.Constraints.Max.Y = gtx.Constraints.Min.Y
	return layout.Flex{}.Layout(gtx, layout.Rigid(dimensionsToWidget(layout.Flex{}.Layout(
		gtx,
		layout.Flexed(0.2, page.widgetBtnSwap),
		layout.Flexed(0.01, layout.Spacer{Width: unit.Dp(2)}.Layout),
		layout.Flexed(0.2, page.widgetButtonRoute),
	))))
}

func (page *PageRoute) widgetButtonRoute(gtx layout.Context) layout.Dimensions {
	if page.clickableRoute.Clicked(gtx) {
		go func() {
			now := time.Now()
			page.TimerStartMobroute = &now
			defer func() {
				page.TimerStartMobroute = nil
				page.UIState.Window.Invalidate()
			}()
			mobrouteResult, err := connmobroute.MobrouteExecuteRoute(connmobroute.MobrouteRequest{
				Sources:            page.UIState.Sources,
				MaxWalkMinutes:     page.UIState.MaxWalkMinutes,
				MaxTripMinutes:     page.UIState.MaxTripMinutes,
				MinTransferMinutes: page.UIState.MinTransferMinutes,
				MaxTransferMinutes: page.UIState.MaxTransferMinutes,
				WalkspeedKmHr:      page.UIState.WalkspeedKmHr,
				LatLonFrom:         page.UIState.LocationFromCoords,
				LatLonTo:           page.UIState.LocationToCoords,
				TransfersTable:     page.UIState.TransfersTable,
				TransfersGenerated: page.UIState.TransfersComputed,
			})
			page.ResultMobrouteResponse = mobrouteResult
			page.ResultMobrouteError = err
		}()
	}
	return material.Button(page.UIState.Theme, &page.clickableRoute, "Route").Layout(gtx)
}

func (page *PageRoute) widgetBtnSwap(gtx layout.Context) layout.Dimensions {
	if page.clickableSwap.Clicked(gtx) {
		swapCoords := page.UIState.LocationFromCoords
		swapLabel := page.UIState.LocationFromLabel
		page.UIState.LocationFromLabel = page.UIState.LocationToLabel
		page.UIState.LocationFromCoords = page.UIState.LocationToCoords
		page.UIState.LocationToLabel = swapLabel
		page.UIState.LocationToCoords = swapCoords
	}

	return material.Button(
		page.UIState.Theme,
		&page.clickableSwap, "Swap",
	).Layout(gtx)
}
