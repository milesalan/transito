package pageroute

import (
	"fmt"

	"gioui.org/layout"
	"gioui.org/unit"
	"gioui.org/x/richtext"
)

func (page *PageRoute) widgetRowSplitToFrom(gtx layout.Context) layout.Dimensions {
	gtx.Constraints.Min.Y = gtx.Dp(unit.Dp(50))
	//gtx.Constraints.Max.Y = gtx.Constraints.Min.Y
	defaultLabel := "Click above to set"

	fromSpans := []richtext.SpanStyle{}
	fromLabel := defaultLabel
	if page.UIState.LocationFromLabel != "" {
		fromLabel = fmt.Sprintf(
			"%s\n\n([%.5f, %.5f](geo:%.5f,%.5f))",
			page.UIState.LocationFromLabel,
			page.UIState.LocationFromCoords[0],
			page.UIState.LocationFromCoords[1],
			page.UIState.LocationFromCoords[0],
			page.UIState.LocationFromCoords[1],
		)
	}
	if s, err := page.UIState.MarkdownRenderer.Render([]byte(fmt.Sprintf(
		"[**From:**](transito://tab1)\n\n%s",
		fromLabel,
	))); err == nil {
		fromSpans = s
	}

	toSpans := []richtext.SpanStyle{}
	toLabel := defaultLabel
	if page.UIState.LocationToLabel != "" {
		toLabel = fmt.Sprintf(
			"%s\n\n([%.5f, %.5f](geo:%.5f,%.5f))",
			page.UIState.LocationToLabel,
			page.UIState.LocationToCoords[0],
			page.UIState.LocationToCoords[1],
			page.UIState.LocationToCoords[0],
			page.UIState.LocationToCoords[1],
		)
	}
	if s, err := page.UIState.MarkdownRenderer.Render([]byte(fmt.Sprintf(
		"[**To:**](transito://tab1)\n\n%s",
		toLabel,
	))); err == nil {
		toSpans = s
	}

	return layout.Flex{Axis: layout.Horizontal}.Layout(gtx,
		layout.Flexed(1, richtext.Text(&page.richtextFrom, page.UIState.Theme.Shaper, fromSpans...).Layout),
		layout.Flexed(0.01, layout.Spacer{Width: unit.Dp(2)}.Layout),
		layout.Flexed(1, richtext.Text(&page.richtextTo, page.UIState.Theme.Shaper, toSpans...).Layout),
	)
}
