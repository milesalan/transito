package utilfix

import (
	"os/exec"
	"strings"
	"time"
)

// TimezoneFixAndroid corrects the local timezone on Android. On Android / for
// termux there is a bug where the timezone is always incorrectly reported as
// UTC; see: https://github.com/golang/go/issues/20455
func TimezoneFixAndroid() {
	if out, err := exec.Command("/system/bin/getprop", "persist.sys.timezone").Output(); err != nil {
		return
	} else if zone, err := time.LoadLocation(strings.TrimSpace(string(out))); err != nil {
		return
	} else {
		time.Local = zone
	}
}
