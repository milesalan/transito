package ui

import (
	"gioui.org/layout"
)

func wgToWidget(ui *UI, wg func(ui *UI, gtx layout.Context) layout.Dimensions) func(layout.Context) layout.Dimensions {
	return func(gtx layout.Context) layout.Dimensions {
		return wg(ui, gtx)
	}
}

func dimensionsToWidget(d layout.Dimensions) func(gtx layout.Context) layout.Dimensions {
	return func(gtx layout.Context) layout.Dimensions {
		return d
	}
}

func truncate(s string, n int) string {
	if len(s) <= n {
		return s
	}
	for i := range s {
		if n == 0 {
			return s[:i]
		}
		n--
	}
	return s
}
