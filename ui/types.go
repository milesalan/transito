package ui

import (
	"gioui.org/layout"
	"gioui.org/widget"
	"git.sr.ht/~mil/transito/uipages/pageconfig"
	"git.sr.ht/~mil/transito/uipages/pageroute"
	"git.sr.ht/~mil/transito/uipages/pagesearch"
	"git.sr.ht/~mil/transito/uistate"
)

type UI struct {
	UIState uistate.UIState

	tabContentList layout.List
	tabButton0     widget.Clickable
	tabButton1     widget.Clickable
	tabButton2     widget.Clickable

	pageConfig pageconfig.PageConfig
	pageRoute  pageroute.PageRoute
	pageSearch pagesearch.PageSearch
}
