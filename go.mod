module git.sr.ht/~mil/transito

go 1.19

//replace git.sr.ht/~mil/mobroute => ../mobroute
//replace git.sr.ht/~mil/mobsql => ../mobsql

require (
	gioui.org v0.4.2
	gioui.org/x v0.4.0
	git.sr.ht/~mil/mobroute v0.3.1-0.20240129023731-1e400aeba8f8
	golang.org/x/exp v0.0.0-20230905200255-921286631fa9 // indirect
	golang.org/x/exp/shiny v0.0.0-20220906200021-fcb1a314c389 // indirect
	golang.org/x/image v0.7.0 // indirect
)

require github.com/inkeliz/giohyperlink v0.0.0-20220903215451-2ac5d54abdce

require (
	gioui.org/cpu v0.0.0-20210817075930-8d6a761490d2 // indirect
	gioui.org/shader v1.0.8 // indirect
	git.sr.ht/~mil/mobsql v0.3.1-0.20240128190433-cd48489bc3ee // indirect
	git.wow.st/gmp/jni v0.0.0-20210610011705-34026c7e22d0 // indirect
	github.com/go-text/typesetting v0.0.0-20230803102845-24e03d8b5372 // indirect
	github.com/jmoiron/sqlx v1.3.5 // indirect
	github.com/mattn/go-sqlite3 v1.14.20 // indirect
	github.com/yuin/goldmark v1.4.13 // indirect
	golang.org/x/sys v0.12.0 // indirect
	golang.org/x/text v0.9.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
