#!/usr/bin/env sh
git remote remove multi
git remote remove origin
git remote remove srht
git remote remove codeberg

git remote add srht git@git.sr.ht:~mil/transito

git remote add codeberg git@codeberg.org:milesalan/transito

git remote add multi git@git.sr.ht:~mil/transito
git remote set-url --add --push multi git@git.sr.ht:~mil/transito
git remote set-url --add --push multi git@codeberg.org:milesalan/transito

cat .git/config
