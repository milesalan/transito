package utilfuncs

import (
	"strconv"
	"strings"
)

func ParseCsvToInts(csv string) ([]int, error) {
	r := []int{}
	for _, v := range strings.Split(csv, ",") {
		if intValue, err := strconv.Atoi(v); err != nil {
			return nil, err
		} else {
			r = append(r, intValue)
		}
	}
	return r, nil
}

func MinFloat64(a, b float64) float64 {
	if a < b {
		return a
	} else {
		return b
	}
}

func MaxFloat64(a, b float64) float64 {
	if a > b {
		return a
	} else {
		return b
	}
}
