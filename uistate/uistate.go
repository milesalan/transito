// Package uistate holds the types for the ui state
package uistate

import (
	"gioui.org/app"
	"gioui.org/widget/material"
	"gioui.org/x/markdown"
)

type UIState struct {
	Window           *app.Window
	Theme            *material.Theme
	MarkdownRenderer *markdown.Renderer
	TabSelected      int

	// Advanced Params from Config Page
	TransfersComputed  bool
	TransfersTable     bool
	VerboseOutput      bool
	MaxWalkMinutes     string
	MaxTripMinutes     string
	MinTransferMinutes string
	MaxTransferMinutes string
	WalkspeedKmHr      string

	// Basic Params from Config Page
	Sources string

	// To/From
	LocationFromCoords []float64
	LocationFromLabel  string
	LocationToLabel    string
	LocationToCoords   []float64
}
